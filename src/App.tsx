import React from 'react';
import './App.css';
import DiscountCalculator from './pages';

function App() {
  return (
    <div className="App">
      <DiscountCalculator />
    </div>
  );
}

export default App;
