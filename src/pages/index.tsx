import { Button, Col, Form, InputNumber, Row } from 'antd';
import React from 'react'

const DiscountCalculator: React.FC = () => {
    const categories: Array<string> = ['a', 'b', 'c', 'd', 'e'];

    const [form] = Form.useForm();

    const onFinish = (values: any) => {
        console.log(values)
    }

    return (
        <div>
            <Row justify="center">
                <Col span={2}>Category</Col>
                <Col span={2}>Qty</Col>
            </Row>

            <Form
                form={form}
                name="discountCalc"
                onFinish={onFinish}
            >
                {categories.length > 0 && categories.map((item, index) => (
                    <Row justify="center" key={index}>
                        <Col span={2}>{item.toUpperCase()}</Col>
                        <Col span={2}>
                            <Form.Item name={item} rules={[{ type: 'integer' }]}>
                                <InputNumber min={1} />
                            </Form.Item>
                        </Col>
                    </Row>
                ))}
                <Row justify="center">
                    <Col span={2}>
                        <Button
                            type="primary"
                            htmlType="submit"
                        >
                            Calculate
                        </Button>
                    </Col>
                    <Col span={2}>
                        <Button onClick={() => form.resetFields()}>Reset</Button>
                    </Col>
                </Row>
            </Form>
        </div >
    )
}
export default DiscountCalculator;